<?php

namespace Drupal\australian_government_crest_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Government Crest' Block
 *
 * @Block(
 *   id = "australian_government_crest_block",
 *   admin_label = @Translation("Government Crest"),
 * )
 */
 
class AustralianGovernmentCrestBlockBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {
  
    $config = $this->getConfiguration();
	
	if (!empty($config['crest_block_link'])) { $crest_block_link = $config['crest_block_link']; } else { $crest_block_link = $this->t('/'); }
	if (!empty($config['crest_block_first'])) { $crest_block_first = $config['crest_block_first']; } else { $crest_block_first = $this->t('Australian Government'); }

	$borderline1 = "";
	$borderline2 = "";
	$borderline3 = "";
	$borderline4 = "";

	if($config['crest_block_select'] == 1) { $borderline1 = "crest-border-none"; }
	if($config['crest_block_select'] == 2) { $borderline2 = "crest-border-none"; }
	if($config['crest_block_select'] == 3) { $borderline3 = "crest-border-none"; }
	if($config['crest_block_select'] == 4) { $borderline4 = "crest-border-none"; }

	if($config['crest_block_color'] == 0) {
	  $crestcolor = 'black';
	} else {
	  $crestcolor = 'white';
	}

	$blockContent = '
	<div class="crest-container crest-layout-' . $config['crest_block_layout'] . '">
	<div class="crest">
		<a href="' . $config['crest_block_link'] . '">
			<img alt="Australian Government" class="size' . $config['crest_block_size'] . '" src="/' . drupal_get_path('module', 'australian_government_crest_block') . '/images/crest-' . $crestcolor . '-' . $config['crest_block_size'] . '.png" />
		</a>
	</div>
	';

	$blockContent .= '
	<div class="titles ' . $crestcolor . ' size' . $config['crest_block_size'] . '">
		<a href="' . $config['crest_block_link'] . '">
	';

	$blockContent .= '
		<span class="line1 size' . $config['crest_block_size'] . ' ' . $borderline1 . '">' . $config['crest_block_first'] . '</span>
	';

	if($config['crest_block_select'] >= 2) {
		$blockContent .= '
		<span class="line2 size' . $config['crest_block_size'] . ' ' . $borderline2 . '">' . $config['crest_block_second'] . '</span>
	';
	}

	if($config['crest_block_select'] >= 3) {
		$blockContent .= '
		<span class="line3 size' . $config['crest_block_size'] . ' ' . $borderline3 . '">' . $config['crest_block_third'] . '</span>
	';
	}

	if($config['crest_block_select'] >= 4) {
		$blockContent .= '
		<span class="line4 size' . $config['crest_block_size'] . ' ' . $borderline4 . '">' . $config['crest_block_fourth'] . '</span>
	';
	}

	$blockContent .= '
		</a>
	</div>
	</div>
	';
	
	$module_path = drupal_get_path('module', 'australian_government_crest_block');
	
    return array(
      '#markup' => $blockContent,
	  '#attached' => array(
        'library' =>  array(
          'australian_government_crest_block/australian_government_crest_block'
        ),
      ),
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
	
    $colorarray = array(0 => t('Black'), 1 => t('White'));
  
    $form['crest_block_color'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Colour of Crest'),
      '#default_value' => isset($config['crest_block_color']) ? $config['crest_block_color'] : '',
      '#options' => $colorarray,
      '#description' => $this->t("Indicates whether the Crest should be Black or White."),
    );
    
    $sizeArray = array(48 => t('48px'), 64 => t('64px'));
  
    $form['crest_block_size'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Size of Crest'),
      '#default_value' => isset($config['crest_block_size']) ? $config['crest_block_size'] : '',
      '#options' => $sizeArray,
      '#description' => $this->t("Indicates what size the crest should be."),
    );
	
	$stackedArray = array('inline' => t('Inline'), 'stacked' => t('Stacked'));
  
    $form['crest_block_layout'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Layout of Crest'),
      '#default_value' => isset($config['crest_block_layout']) ? $config['crest_block_layout'] : '',
      '#options' => $stackedArray,
      '#description' => $this->t("What layout will the Crest be displayed in?"),
    );
    
    $form['crest_block_link'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Crest Link'),
      '#default_value' => isset($config['crest_block_link']) ? $config['crest_block_link'] : '',
  	  '#description' => $this->t("Where should the Crest Block link to?"),
    );
    
    $selectarray = array(1 => t('One'), 2 => t('Two'), 3 => t('Three'), 4 => t('Four'));
  
     $form['crest_block_select'] = array(
      '#type' => 'select',
      '#title' => $this->t('How many lines should display?'),
      '#default_value' => isset($config['crest_block_select']) ? $config['crest_block_select'] : '',
      '#options' => $selectarray,
      '#description' => $this->t("Indicates how many lines of text you need in the crest."),
    );
	
	$form['crest_block_lines'] = array(
      '#type' => 'fieldset',
	  '#title' => 'Lines',
	  '#title_display' => 'invisible',
    );
    
    $form['crest_block_lines']['crest_block_first'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Line 1'),
      '#default_value' => isset($config['crest_block_first']) ? $config['crest_block_first'] : '',
    );
	
    $form['crest_block_lines']['crest_block_second'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Line 2'),
      '#default_value' => isset($config['crest_block_second']) ? $config['crest_block_second'] : '',
    );

    $form['crest_block_lines']['crest_block_third'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Line 3'),
      '#default_value' => isset($config['crest_block_third']) ? $config['crest_block_third'] : '',
    );

    $form['crest_block_lines']['crest_block_fourth'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Line 4'),
      '#default_value' => isset($config['crest_block_fourth']) ? $config['crest_block_fourth'] : '',
    );

	
	$form['crest_block_info'] = array(
      '#markup' => '<p>Please be sure that your website, and the area this block appears in, is in compliance with the <a target="_blank" href="https://www.dta.gov.au/standard/design-guides/branding/">Digital Transformation Agencies branding guidelines</a>.</p>',
    );

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('crest_block_color', $form_state->getValue('crest_block_color'));
	$this->setConfigurationValue('crest_block_size', $form_state->getValue('crest_block_size'));
	$this->setConfigurationValue('crest_block_link', $form_state->getValue('crest_block_link'));
	$this->setConfigurationValue('crest_block_select', $form_state->getValue('crest_block_select'));
	$this->setConfigurationValue('crest_block_first', $form_state->getValue(array('crest_block_lines', 'crest_block_first')));
	$this->setConfigurationValue('crest_block_second', $form_state->getValue(array('crest_block_lines', 'crest_block_second')));
	$this->setConfigurationValue('crest_block_third', $form_state->getValue(array('crest_block_lines', 'crest_block_third')));
	$this->setConfigurationValue('crest_block_fourth', $form_state->getValue(array('crest_block_lines', 'crest_block_fourth')));
	$this->setConfigurationValue('crest_block_layout', $form_state->getValue('crest_block_layout'));
  }
  
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('australian_government_crest_block.settings');
    return array(
      'crest_block_color' => $default_config->get('australian_government_crest_block.crest_block_color'),
	  'crest_block_size' => $default_config->get('australian_government_crest_block.crest_block_size'),
	  'crest_block_link' => $default_config->get('australian_government_crest_block.crest_block_link'),
	  'crest_block_select' => $default_config->get('australian_government_crest_block.crest_block_select'),
	  'crest_block_first' => $default_config->get('australian_government_crest_block.crest_block_first'),
	  'crest_block_layout' => $default_config->get('australian_government_crest_block.crest_block_layout'),
    );
  }
}

?>